package com.arkxis.bean;

public class XMLDescriptorBean 
{
	private String institution;
	private String source;
	private String department;
	private String procedureType;
	private String primaryMRN;
	private String secondaryMRN;
	private String lastName;
	private String firstName;
	private String dateOfBirth;
	private String sex;
	private String fileNamePDF;
	private String filePathPDF;
	private String fileType;
	
	public String getInstitution() {
		return institution;
	}
	public void setInstitution(String institution) {
		this.institution = institution;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getProcedureType() {
		return procedureType;
	}
	public void setProcedureType(String procedureType) {
		this.procedureType = procedureType;
	}
	public String getPrimaryMRN() {
		return primaryMRN;
	}
	public void setPrimaryMRN(String primaryMRN) {
		this.primaryMRN = primaryMRN;
	}
	public String getSecondaryMRN() {
		return secondaryMRN;
	}
	public void setSecondaryMRN(String secondaryMRN) {
		this.secondaryMRN = secondaryMRN;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getFileNamePDF() {
		return fileNamePDF;
	}
	public void setFileNamePDF(String fileNamePDF) {
		this.fileNamePDF = fileNamePDF;
	}
	public String getFilePathPDF() {
		return filePathPDF;
	}
	public void setFilePathPDF(String filePathPDF) {
		this.filePathPDF = filePathPDF;
	}
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
}
