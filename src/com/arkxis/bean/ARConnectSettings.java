package com.arkxis.bean;

import java.util.Objects;

public class ARConnectSettings {
    private String source_path;
    private String source_user;
    private String source_password;
    private String source_descriptor;
    private String destination_user;
    private String destination_path;
    private String destination_password;
    private Integer mode;
    private Integer version;

    public ARConnectSettings() {

    }

    public String getSource_path() {
        return source_path;
    }

    public void setSource_path(String source_path) {
        this.source_path = source_path;
    }

    public String getSource_user() {
        return source_user;
    }

    public void setSource_user(String source_user) {
        this.source_user = source_user;
    }

    public String getSource_password() {
        return source_password;
    }

    public void setSource_password(String source_password) {
        this.source_password = source_password;
    }

    public String getSource_descriptor() {
        return source_descriptor;
    }

    public void setSource_descriptor(String source_descriptor) {
        this.source_descriptor = source_descriptor;
    }

    public String getDestination_user() {
        return destination_user;
    }

    public void setDestination_user(String destination_user) {
        this.destination_user = destination_user;
    }

    public String getDestination_path() {
        return destination_path;
    }

    public void setDestination_path(String destination_path) {
        this.destination_path = destination_path;
    }

    public String getDestination_password() {
        return destination_password;
    }

    public void setDestination_password(String destination_password) {
        this.destination_password = destination_password;
    }

    public Integer getMode() {
        return mode;
    }

    public void setMode(Integer mode) {
        this.mode = mode;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ARConnectSettings settings = (ARConnectSettings) o;
        return Objects.equals(source_path, settings.source_path) &&
                Objects.equals(source_user, settings.source_user) &&
                Objects.equals(source_password, settings.source_password) &&
                Objects.equals(source_descriptor, settings.source_descriptor) &&
                Objects.equals(destination_user, settings.destination_user) &&
                Objects.equals(destination_path, settings.destination_path) &&
                Objects.equals(destination_password, settings.destination_password) &&
                Objects.equals(mode, settings.mode) &&
                Objects.equals(version, settings.version);
    }

    @Override
    public int hashCode() {
        return Objects.hash(source_path, source_user, source_password, source_descriptor, destination_user, destination_path, destination_password, mode, version);
    }

    @Override
    public String toString() {
        return "ARConnectSettings{" +
                "source_path='" + source_path + '\'' +
                ", source_user='" + source_user + '\'' +
                ", source_password='" + source_password + '\'' +
                ", source_descriptor='" + source_descriptor + '\'' +
                ", destination_user='" + destination_user + '\'' +
                ", destination_path='" + destination_path + '\'' +
                ", destination_password='" + destination_password + '\'' +
                ", mode=" + mode +
                ", version=" + version +
                '}';
    }
}

