package com.arkxis.db;

import com.arkxis.bean.ARConnectSettings;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.arkxis.Encryption;

import java.io.File;

public class Database {
    private static final Logger logger = LogManager.getLogger(Database.class);
    private static final String DB_FILE = "arconnect_settings";
    private static final String ENCRYPTION_KEY = "arconfigure_wxyz";

    private Encryption encryption;
    private ObjectMapper objectMapper;

    public Database() {
        encryption = new Encryption(ENCRYPTION_KEY);
        objectMapper = new ObjectMapper();
    }

    public ARConnectSettings getSettings() throws Exception {
        ARConnectSettings settings = objectMapper.readValue(new File(DB_FILE), ARConnectSettings.class);

        if (settings.getVersion() == 1) {
            settings.setSource_descriptor(encryption.decrypt(settings.getSource_descriptor()));
            settings.setSource_path(encryption.decrypt(settings.getSource_path()));
            settings.setSource_user(encryption.decrypt(settings.getSource_user()));
            settings.setSource_password(encryption.decrypt(settings.getSource_password()));
            settings.setDestination_path(encryption.decrypt(settings.getDestination_path()));
            settings.setDestination_user(encryption.decrypt(settings.getDestination_user()));
            settings.setDestination_password(encryption.decrypt(settings.getDestination_password()));
        }

        return settings;
    }
}