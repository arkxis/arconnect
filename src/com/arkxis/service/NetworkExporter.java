package com.arkxis.service;

import java.util.*;
import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.*;
import java.sql.Timestamp;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.OutputKeys;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import static java.nio.file.FileVisitResult.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.arkxis.util.FileUtil;
import com.arkxis.net.CredentialsBean;
import com.arkxis.bean.XMLDescriptorBean;

public class NetworkExporter 
	extends SimpleFileVisitor<Path>
{
	private static final Logger logger = LogManager.getLogger(NetworkExporter.class.getName());
	private static final int PDF_ONLY = 1;
	private static final int FILE_RENAME = 2;
	private static final int PDF_FLAT = 3;
	private static final int TAGGED_INDEX_PDF = 4;
	private static final int PREFIX_EXPORT = 5;
	private static final int PREFIX_EXPORT_PDF_ONLY = 6;
	
	private int transferFunction;
	private String descriptorName;
	private File destPath;
	private CredentialsBean credentials;
	private FileUtil fileUtil;
	private String destFileName;
	private String destPDFFileName;
	
	public NetworkExporter(int transFunc, String sourceDescriptorName, CredentialsBean creds, File destFile)
	{
		transferFunction = transFunc;
		descriptorName = sourceDescriptorName;
		credentials = creds;
		destPath = destFile;
		
		fileUtil = new FileUtil();
	}
		
	private boolean createTaggedIndexFile(File xmlFile, File pdf, String fullPath)
	{
		boolean success = false;
		
		logger.info("Creating Tagged Index file based on XML File: "+xmlFile);
		
		try
		{
			XMLDescriptorBean xmlDescriptorBean = new XMLDescriptorBean();
					
			Document xmlDoc;

			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			
			xmlDoc = builder.parse(xmlFile.getAbsolutePath());
			xmlDoc.getDocumentElement().normalize();

			Node patient = xmlDoc.getElementsByTagName("patient").item(0);
			
			Element mrnElement = (Element)patient;
			String mrn = mrnElement.getElementsByTagName("mrn").item(0).getTextContent();
			
			StringTokenizer st = new StringTokenizer(mrn, "..");
			String primaryMRN = st.nextToken();
			String secondaryMRN = st.nextToken();
			
			//PDF File Name format is <CSN>_<TIMESTAMP>.pdf
			String updatedFileName = primaryMRN+"_";
			
			/*
			String[] tokens = pdf.getName().split("_");
			
			if(tokens.length > 4)
				updatedFileName = updatedFileName.concat(tokens[4]);
			else
			{
				logger.error("Unable to retrieve file timestamp, will use current timestamp");
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				updatedFileName = updatedFileName.concat(Long.toString(timestamp.getTime()));
			}
			*/
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			updatedFileName = updatedFileName.concat(Long.toString(timestamp.getTime()));
			
			destPDFFileName = updatedFileName.concat(".pdf");
			
			xmlDescriptorBean.setPrimaryMRN(primaryMRN);
			xmlDescriptorBean.setSecondaryMRN(secondaryMRN);
			xmlDescriptorBean.setProcedureType(xmlDoc.getElementsByTagName("procedure_type").item(0).getTextContent());
			xmlDescriptorBean.setLastName(xmlDoc.getElementsByTagName("lastname").item(0).getTextContent());
			xmlDescriptorBean.setFirstName(xmlDoc.getElementsByTagName("firstname").item(0).getTextContent());
			xmlDescriptorBean.setDateOfBirth(xmlDoc.getElementsByTagName("dob").item(0).getTextContent());
			xmlDescriptorBean.setSex(xmlDoc.getElementsByTagName("sex").item(0).getTextContent());
			xmlDescriptorBean.setFilePathPDF(fullPath);
			xmlDescriptorBean.setFileNamePDF("\\"+destPDFFileName);
			
			/*
			logger.info("MRN: "+mrn);
			logger.info("Primary MRN: "+primaryMRN);
			logger.info("Secondary MRN: "+secondaryMRN);
			logger.info("Description: "+xmlDescriptorBean.getProcedureType());
			logger.info("MRN: "+xmlDescriptorBean.getSecondaryMRN());
			logger.info("Last Name: "+xmlDescriptorBean.getLastName());
			logger.info("First Name: "+xmlDescriptorBean.getFirstName());
			logger.info("DOB: "+xmlDescriptorBean.getDateOfBirth());
			logger.info("Gender: "+xmlDescriptorBean.getSex());
			logger.info("CSN: "+xmlDescriptorBean.getPrimaryMRN());
			logger.info("File Path: "+xmlDescriptorBean.getFilePathPDF());
			logger.info("File Name: "+xmlDescriptorBean.getFileNamePDF());
			*/
			
			String dipFile = xmlFile.getAbsolutePath().replace("descriptor.xml", updatedFileName.concat(".DIP"));
			destFileName = dipFile;
			
			try (BufferedWriter bw = new BufferedWriter(new FileWriter(dipFile))) 
			{
				String content = 
						">>>>Self Configuring Tagged DIP<<<<\r\n"+
						"BEGIN:\r\n"+
						">>DocType: Photographs - Scan\r\n"+
						"Description: "+xmlDescriptorBean.getProcedureType()+"\r\n"+
						"MRN: "+xmlDescriptorBean.getSecondaryMRN()+"\r\n"+
						"Last Name: "+xmlDescriptorBean.getLastName()+"\r\n"+
						"First Name: "+xmlDescriptorBean.getFirstName()+"\r\n"+
						"DOB: "+xmlDescriptorBean.getDateOfBirth()+"\r\n"+
						"Gender: "+xmlDescriptorBean.getSex()+"\r\n"+
						"CSN: "+xmlDescriptorBean.getPrimaryMRN()+"\r\n"+
						"File Path: "+xmlDescriptorBean.getFilePathPDF()+"\r\n"+
						">>FileType: 16\r\n"+
						">>FileName: "+xmlDescriptorBean.getFileNamePDF()+
						"\r\nEND:";

				bw.write(content);
				
				success = true;
			}
			catch (IOException e) 
			{
				logger.error("Error Writing File: "+e);
			}
		}
		catch (Exception e)
		{
			logger.info("error: "+e);
		}
		
		return success;
	}
		
	private boolean updateXML(File xmlFile, File pdf)
	{
		boolean success = false;
		
		logger.info("Updating XML File: "+xmlFile);
		
		//update xmlDoc MRN, ECD, and add PDF filename
		try
		{
			Document xmlDoc;

			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			
			xmlDoc = builder.parse(xmlFile.getAbsolutePath());
			xmlDoc.getDocumentElement().normalize();

			Node patient = xmlDoc.getElementsByTagName("patient").item(0);
			
			Element mrnElement = (Element)patient;
			String mrn = mrnElement.getElementsByTagName("mrn").item(0).getTextContent();
			
			StringTokenizer st = new StringTokenizer(mrn, "..");
			String ecd = st.nextToken();
			String updatedMRN = st.nextToken();

			mrnElement.getElementsByTagName("mrn").item(0).setTextContent(updatedMRN);
			
			logger.info("MRN: "+mrn);
			logger.info("ECD: "+ecd);
			logger.info("New MRN: "+updatedMRN);
			
			//Add ECD Node to Patient 
			Element ecdElement = xmlDoc.createElement("ecd");
			ecdElement.appendChild(xmlDoc.createTextNode(ecd));
			patient.appendChild(ecdElement);
			
			//Remove Picture Nodes
			removeChild(xmlDoc.getElementsByTagName("pictures").item(0));
			
			//Add PDF to Files Node 
			Element fileElement = xmlDoc.createElement("file");
			Element pathElement = xmlDoc.createElement("path");
			pathElement.appendChild(xmlDoc.createTextNode(pdf.getName()));

			fileElement.appendChild(pathElement);
			xmlDoc.getElementsByTagName("files").item(0).appendChild(fileElement);
			
			//write the content into xml file
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
			
			String updatedDescriptor = xmlFile.getAbsolutePath().replaceAll("descriptor", "updatedDescriptor");
			
			DOMSource source = new DOMSource(xmlDoc);
			StreamResult result = new StreamResult(new File(updatedDescriptor));
			transformer.transform(source, result);
			
			success = true;
		}
		catch (Exception e)
		{
			logger.info("error: "+e);
		}
		
		return success;
	}
	
	public static void removeChild(Node node) 
	{
	    while(node.hasChildNodes())
	    	node.removeChild(node.getFirstChild());
	}
	
	private boolean updateFileNamesXML(File xmlFile, List<String> images)
	{
		boolean success = false;
		logger.info("Updating XML File Names: "+xmlFile);
		
		//update Pictures to shorter filenames
		try
		{
			Document xmlDoc;

			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			
			xmlDoc = builder.parse(xmlFile.getAbsolutePath());
			xmlDoc.getDocumentElement().normalize();

			NodeList pictures = xmlDoc.getElementsByTagName("picture");
			
			for(int i = 0; i < pictures.getLength(); i++)
			{
				Element picture = (Element) pictures.item(i);
				picture.getElementsByTagName("path").item(0).setTextContent(images.get(i));
				picture.getElementsByTagName("description").item(0).setTextContent(images.get(i));
			}
			
			//write the content into xml file
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
			
			String updatedDescriptor = xmlFile.getAbsolutePath().replaceAll("descriptor", "updatedDescriptor");
			
			DOMSource source = new DOMSource(xmlDoc);
			StreamResult result = new StreamResult(new File(updatedDescriptor));
			transformer.transform(source, result);
			
			success = true;
		}
		catch (Exception e)
		{
			logger.info("error: "+e);
		}
		
		return success;
	}
	
	private void performFileRenameExport(Path path)
	{
		File drDirectory = path.getParent().getParent().toFile();
		File patientDirectory = path.getParent().toFile();
		
		List<String> media = new ArrayList<String>();
		List<File> renamedFiles = new ArrayList<File>();
		
		File[] files = patientDirectory.listFiles();
		
		for(File file : files) 
		{
		    if(file.getName().endsWith(".jpg")) 
		    {
		    	String newName = file.getName().substring(file.getName().indexOf("Still_"));
		    	file.renameTo(new File(patientDirectory+"\\"+newName));
		    	
		    	media.add(newName);
		    	renamedFiles.add(new File(patientDirectory+"\\"+newName));
		    }
		    else if(file.getName().endsWith(".mp4"))
		    {
		    	String newName = file.getName().substring(file.getName().indexOf("Video_"));
		    	file.renameTo(new File(patientDirectory+"\\"+newName));
		    	
		    	media.add(newName);
		    	renamedFiles.add(new File(patientDirectory+"\\"+newName));
		    }
		}
		
		if(!media.isEmpty())
		{
			File xmlFile = path.toFile();
			
			// Update XML File
			if(updateFileNamesXML(xmlFile, media))
			{
				String docDir = drDirectory.getAbsolutePath().substring(drDirectory.getAbsolutePath().lastIndexOf("\\"));
				String patDir = patientDirectory.getAbsolutePath().substring(patientDirectory.getAbsolutePath().lastIndexOf("\\"));

				File updatedXMLFile = new File(xmlFile.getAbsolutePath().replace("descriptor", "updatedDescriptor"));

				// Add full directory path and format to Samba
				String formattedDest = "smb:"+ (destPath.getAbsolutePath() + docDir + patDir).replace('\\', '/');

				logger.info("Formatted Destination: " + formattedDest);

				fileUtil.exportAndRenameFile(credentials, updatedXMLFile, formattedDest, updatedXMLFile.getName().replace("updatedDescriptor","descriptor"));
				fileUtil.exportFiles(credentials, renamedFiles, formattedDest);
				
				fileUtil.deleteDirectoryFiles(patientDirectory);
			} 
			else
				logger.error("Unable to update XML File");
		}
		else
			logger.error("No media found for: "+path);
	}
		
	private void performPDFExport(boolean nested, boolean taggedIndex, Path path, String prefix)
	{
		File drDirectory = path.getParent().getParent().toFile();
		File patientDirectory = path.getParent().toFile();

		// Make sure corresponding PDF is present
		File pdf = fileUtil.containsPDF(patientDirectory);

		if(pdf != null) 
		{
			String docDir = drDirectory.getAbsolutePath().substring(drDirectory.getAbsolutePath().lastIndexOf("\\"));
			String patDir = patientDirectory.getAbsolutePath().substring(patientDirectory.getAbsolutePath().lastIndexOf("\\"));
			
			File xmlFile = path.toFile();
			
			if(taggedIndex)
			{
				if(createTaggedIndexFile(xmlFile, pdf, destPath.getAbsolutePath()))
				{
					File taggedIndexFile = new File(destFileName);
					
					String formattedDest = "smb:"+ (destPath.getAbsolutePath()).replace('\\', '/');
					
					logger.info("Formatted Destination: "+formattedDest);
				
					fileUtil.exportAndRenameFile(credentials, pdf, formattedDest, destPDFFileName);
					
					List<File> files = new ArrayList<File>();
					files.add(taggedIndexFile);
					fileUtil.exportFiles(credentials, files, formattedDest);
					
					fileUtil.deleteDirectoryFiles(patientDirectory);					
				}
				else
					logger.error("Unable to create Tagged Index File");
			}
			else
			{
				if(prefix != null)
				{
					logger.info("Adding Prefix value to PDF: "+prefix);
					String newFileName = prefix+pdf.getName();								
					File newFile = new File(patientDirectory+"\\"+newFileName);
					
					try{
						pdf = Files.move(pdf.toPath(), newFile.toPath()).toFile();
					}
					catch(IOException e){
						logger.error(e);
					}				

					logger.info("Updated PDF Value: "+pdf.getAbsolutePath());
				}
				
				// Update XML File
				if(updateXML(xmlFile, pdf)) 
				{
					File updatedXMLFile = new File(xmlFile.getAbsolutePath().replace("descriptor", "updatedDescriptor"));

					String formattedDest;
					String descriptorName;
					
					if(nested) // Add full directory path and format
					{
						formattedDest = "smb:"+ (destPath.getAbsolutePath() + docDir + patDir).replace('\\', '/');
						descriptorName = "descriptor";
					}
					else // Add only root folder and change descriptor name
					{
						//If adding a prefix, the timestamp is the 5th token otherwise it is the 4th
						int tstoken = (prefix != null) ? 5 : 4;
													
						formattedDest = "smb:"+ (destPath.getAbsolutePath()).replace('\\', '/');
						String[] tokens = pdf.getName().split("_");
						
						if(tokens.length > tstoken)
							descriptorName = "descriptor_"+tokens[tstoken];
						else
						{
							logger.error("Unable to retrieve file timestamp, will use current timestamp");
							Timestamp timestamp = new Timestamp(System.currentTimeMillis());
							descriptorName = Long.toString(timestamp.getTime());
						}
					}

					logger.info("Formatted Destination: " + formattedDest);

					fileUtil.exportAndRenameFile(credentials, updatedXMLFile, formattedDest, updatedXMLFile.getName().replace("updatedDescriptor",descriptorName));

					List<File> files = new ArrayList<File>();
					files.add(pdf);
					fileUtil.exportFiles(credentials, files, formattedDest);

					fileUtil.deleteDirectoryFiles(patientDirectory);
				} 
				else
					logger.error("Unable to update XML File");
			}
		} 
		else
			logger.error("PDF does not exist in: " + patientDirectory);
	}
	
	@Override
    public FileVisitResult visitFile(Path path, BasicFileAttributes attrs) 
	{	
		//Check if there is a descriptor XML file
		if(path.getFileName().toString().equals(descriptorName))
		{
			logger.info("Matching XML File: "+path.toFile().getAbsolutePath());
			logger.info("Performing Transfer Function: "+transferFunction);
			
				switch(transferFunction)
				{
					case PDF_ONLY:
						performPDFExport(true, false, path, null);
						break;
						
					case FILE_RENAME:
						performFileRenameExport(path);
						break;
						
					case PDF_FLAT:
						performPDFExport(false, false, path, null);
						break;
						
					case TAGGED_INDEX_PDF:
						performPDFExport(false, true, path, null);
						break;
					case PREFIX_EXPORT:
						performPDFExport(true, false, path, "OP9414_");
						break;
					case PREFIX_EXPORT_PDF_ONLY:
						performPDFExport(false, false, path, "OP9414_");
						break;	
			        default :
			        	logger.info("Invalid Transfer Function: "+transferFunction);
				}
		}
        return CONTINUE;
    }
	
	 @Override
     public FileVisitResult visitFileFailed(Path file, IOException exc) 
	 {
		 logger.error(exc);
         return CONTINUE;
	 }
}
