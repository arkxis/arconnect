package com.arkxis.service;

import java.util.Objects;
import java.io.*;
import java.nio.file.*;
import java.util.regex.Pattern;

import static java.nio.file.LinkOption.NOFOLLOW_LINKS;

import com.arkxis.bean.ARConnectSettings;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import com.arkxis.net.CredentialsBean;
import com.arkxis.db.Database;


public class ARConnectService 
{
	private static final Logger logger = LogManager.getLogger(ARConnectService.class.getName());

	private static class Settings {
		private String sourceFilePath;
		private String sourceDescriptorName;
		private String destFilePath;
		private String destDomain;
		private String destUser;
		private String destPassword;
		private Integer transferFunction;

		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (o == null || getClass() != o.getClass()) return false;
			Settings settings = (Settings) o;
			return Objects.equals(sourceFilePath, settings.sourceFilePath) &&
					Objects.equals(sourceDescriptorName, settings.sourceDescriptorName) &&
					Objects.equals(destFilePath, settings.destFilePath) &&
					Objects.equals(destDomain, settings.destDomain) &&
					Objects.equals(destUser, settings.destUser) &&
					Objects.equals(destPassword, settings.destPassword) &&
					Objects.equals(transferFunction, settings.transferFunction);
		}

		@Override
		public int hashCode() {
			return Objects.hash(sourceFilePath, sourceDescriptorName, destFilePath, destDomain, destUser, destPassword, transferFunction);
		}
	}

	private static Settings getSettings() throws Exception 
	{
		ARConnectSettings arconnectSettings = new Database().getSettings();
		
		///////////////////////////////////////////////////////////////
		//arconnectSettings.setSource_path("C:\\LakeHealth");
		//arconnectSettings.setDestination_path("\\\\192.168.1.15\\FacilityExport");
		//arconnectSettings.setMode(5);
		///////////////////////////////////////////////////////////////
		
		Settings settings = new Settings();

		settings.sourceFilePath = arconnectSettings.getSource_path();
		settings.sourceDescriptorName = arconnectSettings.getSource_descriptor();

		settings.destFilePath = arconnectSettings.getDestination_path();
		settings.transferFunction = arconnectSettings.getMode();
		String[] domainAndUser = arconnectSettings.getDestination_user().split(Pattern.quote("\\"));
		settings.destDomain = "";
		settings.destUser = "";

		if (domainAndUser.length > 1) 
		{
			settings.destDomain = domainAndUser[0];
			settings.destUser = domainAndUser[1];
		} 
		else 
			settings.destUser = domainAndUser[0];

		settings.destPassword = arconnectSettings.getDestination_password();
	
		///////////////////////////////////////////////////////////////
		//settings.destUser = "SynergyUser";
		//settings.destPassword = "Arthrex123";
		/////////////////////////////////////////////////////////////////
		
		return settings;
	}

	public static void main(String[] args) throws InterruptedException
	{
		logger.info("ARConnect Service starting - cwd {}", System.getProperty("user.dir"));
		
		long sleepTime = 15000L;
		Settings previousSettings = null;
		
		while (true) 
		{
			try 
			{
				Settings settings = getSettings();
								
				if (!settings.equals(previousSettings)) 
				{
					logger.info("Sleep Time: " + sleepTime);
					logger.info("Source File Path: " + settings.sourceFilePath);
					logger.info("Source Descriptor: " + settings.sourceDescriptorName);
					logger.info("Dest File Path: " + settings.destFilePath);
					logger.info("Dest Transfer Function: " + settings.transferFunction);

					previousSettings = settings;
				}

				File sourcePath = new File(settings.sourceFilePath);
				File destPath = new File(settings.destFilePath);
				CredentialsBean credentials = new CredentialsBean(settings.destDomain, settings.destUser, settings.destPassword);
				
				try 
				{
					Boolean isFolder = (Boolean) Files.getAttribute(sourcePath.toPath(), "basic:isDirectory", NOFOLLOW_LINKS) && Files.isWritable(sourcePath.toPath());

					if (!isFolder) 
					{
						logger.error("Source Path: " + sourcePath + " is not a writable folder");
						continue;
					}
				} 
				catch (IOException ioe) 
				{
					logger.error(ioe);
					continue;
				}

				NetworkExporter networkExporter = new NetworkExporter(settings.transferFunction, settings.sourceDescriptorName, credentials, destPath);
				Files.walkFileTree(sourcePath.toPath(), networkExporter);
			} 
			catch (Exception e) {
				logger.error(e);
			} 
			finally {
				Thread.sleep(sleepTime);
			}
		}
	}
}
