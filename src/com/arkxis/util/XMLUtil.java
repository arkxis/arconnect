package com.arkxis.util;

import java.io.*;
import javax.xml.parsers.DocumentBuilder; 
import javax.xml.parsers.DocumentBuilderFactory; 
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.dom.DOMSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;

public class XMLUtil 
{
	private static final Logger logger = LogManager.getLogger(XMLUtil.class.getName());
	
	public static Document getDocument(File f)
	{
		Document document = null;
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try 
		{
            DocumentBuilder builder = factory.newDocumentBuilder();
            document = builder.parse(f);
            
            logger.info("Parsed Doc: "+document.getFirstChild()  );
        } 
        catch (Exception e) 
		{
        	logger.error(e);
        }
		return document;
	}
	
	public static Document updateXML(Document xmlDoc, String pdf)
	{
		logger.info("Updating Doc: "+xmlDoc);
		logger.info("PDF String: "+pdf);
		
		return xmlDoc;
		
	}
	
	public static String prettyPrint(Document xml) throws Exception 
	{
		Transformer tf = TransformerFactory.newInstance().newTransformer();
		tf.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		tf.setOutputProperty(OutputKeys.INDENT, "yes");
		
		Writer out = new StringWriter();
		tf.transform(new DOMSource(xml), new StreamResult(out));
		
		return out.toString();   
	}
}
